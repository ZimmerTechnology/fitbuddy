package adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zimmertechnology.fitbuddy.AchievedFragment;
import com.zimmertechnology.fitbuddy.CompletedFragment;
import com.zimmertechnology.fitbuddy.RivalFragment;


/**
 * Created by Emmanuel on 3/8/2015.
 */
public class TabPageAdapter extends FragmentPagerAdapter {

    public TabPageAdapter(FragmentManager fragmentManager){
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int index){

        switch (index){
            case 0:
                return new AchievedFragment();
            case 1:
                return new CompletedFragment();
            case 2:
                return new RivalFragment();
        }

        return null;
    }

    @Override
    public int getCount(){

        //we look for how many fragmentsa that are needed
        return 3;
    }
}

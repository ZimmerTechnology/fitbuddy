package com.zimmertechnology.fitbuddy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * Activity which displays a login screen to the user, offering registration as well.
 */
public class Login extends Activity {
    //Declaring variables from login activity
    Button btnlogin;
    Button btnLinkToRegisterScreen;
    EditText email;
    EditText password;
    String emailtxt;
    String passwordtxt;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        //Text for user login
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);


        btnlogin = (Button) findViewById(R.id.btnLogin);
        btnLinkToRegisterScreen = (Button) findViewById(R.id.btnLinkToRegisterScreen);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Retrieve data from  parse.
                emailtxt = email.getText().toString();
                passwordtxt = password.getText().toString();

                //Verify agaist user login table
                ParseUser.logInInBackground(emailtxt,passwordtxt, new LogInCallback() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {
                        if (parseUser != null){
                                //after checking parse table set new activity
                            Intent loggedintruly = new Intent(Login.this, MainActivity.class );
                            startActivity(loggedintruly);
                            Toast.makeText(getApplicationContext(),"Time to find a fitbuddy", Toast.LENGTH_LONG).show();
                            finish();
                        } else{
                            Toast.makeText(getApplicationContext(),"No such user, please signup",Toast.LENGTH_LONG).show();
                        }
                    }
                });

                btnLinkToRegisterScreen.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //Take user to Registration page
                        Intent register = new Intent(Login.this,Registration.class);
                        startActivity(register);
                        finish();
                    }
                });

            }
        });
  }


}

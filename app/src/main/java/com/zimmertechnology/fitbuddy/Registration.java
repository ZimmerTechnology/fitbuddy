package com.zimmertechnology.fitbuddy;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class Registration extends Activity {

    private static final String TAG = Registration.class.getSimpleName();
    private Button btnRegister;
    private Button btnLinkToLogin;
    private EditText inputFullName;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    String inputFullNametxt;
    String inputEmailtxt;
    //Have to run encryption on user credential
    String inputPasswordtxt;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        inputFullName = (EditText) findViewById(R.id.name);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);


        // Link to Login Screen
        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {

                //Retrieve entered data
                inputFullNametxt = inputFullName.getText().toString();
                inputPasswordtxt = inputPassword.getText().toString();
                inputEmailtxt = inputEmail.getText().toString();

               if (inputEmailtxt.equals("") && inputPasswordtxt.equals("") && inputFullNametxt.equals("")){
                   Toast.makeText(getApplicationContext(),
                           "PLease complete the sign up form",
                           Toast.LENGTH_LONG).show();
               }else{

               }
                Intent i = new Intent(getApplicationContext(),
                        Application.class);
                startActivity(i);
                finish();
            }
        });

    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}


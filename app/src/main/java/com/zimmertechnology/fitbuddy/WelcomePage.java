package com.zimmertechnology.fitbuddy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.parse.ParseAnonymousUtils;
import com.parse.ParseUser;


public class WelcomePage extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //determine if whether the current user is a new user or needs to login
        if (ParseAnonymousUtils.isLinked(ParseUser.getCurrentUser())){
            //User is new to fitbuddy send them to login page
            Intent login = new Intent(WelcomePage.this, Login.class );
            startActivity(login);
            finish();
        }else{
            //If this individual is a newbie
            //Get current user data from paRSE
            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser != null){
                //Send user to mainactivity.class
                Intent loggedin = new Intent(WelcomePage.this, MainActivity.class);
                startActivity(loggedin);
                finish();
            }else{
                //Send user back to login if they haven't logged in

                Intent userlogin = new Intent(WelcomePage.this,Login.class);
                startActivity(userlogin);
                finish();
            }


        }
    }

}

package com.zimmertechnology.fitbuddy;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Emmanuel on 3/8/2015.
 */
public class Videos extends Fragment {

    public Videos() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ){
        View rootView = inflater.inflate(R.layout.fragment_video, container, false);

        return rootView;
    }
}


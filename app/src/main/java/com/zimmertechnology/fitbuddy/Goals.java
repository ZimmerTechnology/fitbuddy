package com.zimmertechnology.fitbuddy;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Emmanuel on 3/8/2015.
 */
public class Goals extends Fragment{

    public Goals() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ){
        View rootView = inflater.inflate(R.layout.fragment_goals, container, false);

        return rootView;
    }
}


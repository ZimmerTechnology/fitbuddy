package com.zimmertechnology.fitbuddy;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;

/**
 * Created by Emmanuel on 4/14/2015.
 */
public class ParseApplication extends android.app.Application {


    public ParseApplication() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Parse.initialize(this, "hAgz5nYcA65xDLFXeBWB95VktIMxwN5u5bbGrZQz", "a2b4dxKdS30YYOZ6G0KlGyuB8G05XcJ9e5N6wDWp");

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        // If you would like all objects to be private by default, remove this
        // line.
        defaultACL.setPublicReadAccess(true);

        ParseACL.setDefaultACL(defaultACL, true);
    }


}


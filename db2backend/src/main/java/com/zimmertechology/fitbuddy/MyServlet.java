/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Servlet Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloWorld
*/

package com.zimmertechology.fitbuddy;

import com.google.appengine.api.utils.SystemProperty;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.http.*;


import javax.servlet.http.*;

public class MyServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        resp.setContentType("text/plain");
        resp.getWriter().println("Please use the form to POST to this url");
    }
    //We have two options when calling the sql driver to help establish the connection (jdbc)
    //we can either go googledriver route or just drive, to enable us to make the connection to the server
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        String url = null;

        try {
            if (SystemProperty.environment.value() == SystemProperty.Environment.Value.Production) {

                Class.forName("com.mysql.jdbc.GoogleDriver");
                url = "jdbc:mysql://mimetic-heaven-840:fitbuddy/fitbuddy?user=root";
            } else{
                Class.forName("com.mysql.jdbc.Driver");
                url = "jdbc:mysql://173.194.80.254:3306/fitbuddy?user=root";
            }
        }catch (Exception e){
            e.printStackTrace();
            return;
        }

        //We try to establish a connection here to the google cloud sql server
        //Within this logic we have set two if statements and try catch blocks to check and make sure when it passes and fail
        //When it fails an error message is displayed this error message informs the user that it will try and establish the connection to the again
        PrintWriter out = resp.getWriter();
        try{
            Connection connection = (Connection) DriverManager.getConnection(url);
            try{
                String fname = req.getParameter("fname");
                String content = req.getParameter("content");

                if (fname == " " || content == " "){
                    out.println("<html><head></head><body> You are missing either a message or a name! Try again! " + "Redirecting in 3 seconds....</body></html>");
                } else{
                    String statement = "INSERT INTO entries (Name, content) VALUES(?, ?)";
                    PreparedStatement stmt = connection.prepareStatement(statement);
                    stmt.setString(1, fname);
                    stmt.setString(2, content);
                    int success = 2;
                    success = stmt.executeUpdate();

                    if (success == 1){
                        out.println("<html><head></head><body>Success! Redirecting in 3 seconds...</body></html>");
                    }else if (success == 0){
                        out.println("<html><head></head><body>Failure! Please try again! " +
                                "Redirecting in 3 seconds...</body></html>");
                    }
                }
            }finally{
                connection.close();
            }
        }catch(SQLException e){
            e.printStackTrace();
        }

    }
}
